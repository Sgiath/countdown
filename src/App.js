import React from "react";
//import firebase from "firebase";
//import "firebase/firestore";

import "./App.css";

class App extends React.Component {
  state = {
    seconds: 0,
    events: [
      {
        name: "Test",
        description: "Testing event",
        start: window.firebase.firestore.Timestamp.fromDate(new Date(2018, 11)),
        end: window.firebase.firestore.Timestamp.fromDate(new Date(2019, 1))
      }
    ]
  };

  timeString = () => {
    const hours = Math.floor(this.state.seconds / (60 * 60));

    const divisor_for_minutes = this.state.seconds % (60 * 60);
    const minutes = Math.floor(divisor_for_minutes / 60);

    const divisor_for_seconds = divisor_for_minutes % 60;
    const seconds = Math.ceil(divisor_for_seconds);

    return `${hours
      .toString()
      .padStart(2, "0")} : ${minutes
      .toString()
      .padStart(2, "0")} : ${seconds.toString().padStart(2, "0")}`;
  };

  getCurrentEvent = () => {
    return this.state.events.filter(
      event => event.end > window.firebase.firestore.Timestamp.now()
    )[0];
  };

  getNextEvent = () => {
    return this.state.events.filter(
      event => event.start > window.firebase.firestore.Timestamp.now()
    )[0];
  };

  componentDidMount() {
    this.interval = setInterval(
      () =>
        this.setState({
          seconds:
            this.getNextEvent().start.seconds -
            window.firebase.firestore.Timestamp.now().seconds
        }),
      1000
    );

    window.firebase.initializeApp({
      apiKey: "AIzaSyBoDqdeYJHW6GDDaWcjOewsbkLYXxWbfQo",
      authDomain: "hackathons-ef695.firebaseapp.com",
      projectId: "hackathons-ef695"
    });

    this.db = window.firebase.firestore();
    this.db.settings({ timestampsInSnapshots: true });
    this.db.enablePersistence().catch(err => console.log("Not persistent"));

    this.listener = this.db
      .collection("hackathons")
      .doc("RueQG8dhjXhwvFmvCCS3")
      .collection("timeline")
      .where("end", ">", window.firebase.firestore.Timestamp.now())
      .orderBy("end")
      .onSnapshot(querySnapshot => {
        const events = [];
        querySnapshot.forEach(doc => {
          events.push(doc.data());
        });
        this.setState({ events });
      });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    this.listener();
  }

  render() {
    const currentEvent = this.getCurrentEvent();
    const nextEvent = this.getNextEvent();
    const classes = ["Time"];
    if (
      nextEvent.start.seconds -
        window.firebase.firestore.Timestamp.now().seconds <
      5 * 60
    ) {
      classes.push("Red");
      if (
        nextEvent.start.seconds -
          window.firebase.firestore.Timestamp.now().seconds <
        60
      ) {
        classes.push("Flash");
      }
    }
    return (
      <div className="App">
        <header className="App-header">
          <div className="CurrentEvent">
            <strong>{currentEvent.name}</strong> ends in:
            <br />
            <div className={classes.join(" ")}>{this.timeString()}</div>
          </div>
          <div className="NextEvent">
            Next up: <strong>{nextEvent.name}</strong>
          </div>
          <p style={{ marginTop: "200px" }}>
            <a
              href="https://profiq.page.link/resources"
              target="_blank"
              rel="noopener noreferrer"
            >
              https://profiq.page.link/resources
            </a>
          </p>
        </header>
      </div>
    );
  }
}

export default App;
